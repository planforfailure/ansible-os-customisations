# Ansible Role: O/S Customisations
An Ansible role that deploys O/S customisations for RedHat/OracleLinux & CentOS hosts when using only Ansible core. This is a replacement for VMWare O/S [Customisations](https://docs.vmware.com/en/VMware-vSphere/7.0/com.vmware.vsphere.vm_admin.doc/GUID-9A5093A5-C54F-4502-941B-3F9C0F573A39.html) which falls short when configring post deplyment on VMs and requires specific configuration criteria set that cannot be accomadated in VMWare.


## Requirements
This can be safely ignored unless you are running on Splunk hosts otherwise run as per stated installation steps.
- `splunk-thp`

## Role Variables
All variables are defined using [vars_prompt](https://docs.ansible.com/ansible/latest/playbook_guide/playbooks_prompts.html) alternatively these can defined in `defaults/`.

## Installation
```bash
$ ansible-galaxy install -r requirements.yml

$ ansible-playbook deploy.yml -K
```


## Licence
MIT/BSD
